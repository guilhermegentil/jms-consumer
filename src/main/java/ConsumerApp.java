import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class ConsumerApp {

    public static void main(String[] args) throws Exception{
        QueueConsumer queueConsumer = new QueueConsumer("tcp://localhost:61616", "guest", "guest");

        queueConsumer.startReceiving("test.queue1", new MyMessageListener());
        queueConsumer.startReceiving("test.queue2", new MyMessageListener());
        queueConsumer.startReceiving("test.queue3", new MyMessageListener());
    }

}

class MyMessageListener implements MessageListener{

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            TextMessage txt = (TextMessage) message;
            try {
                System.out.println("Message received =" + txt.getText());
            } catch (JMSException e) {
                System.out.println("error retrieving message");
            }
        }
    }

}
