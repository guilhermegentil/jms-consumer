import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class QueueConsumer {

    private String brokerUrl;
    private String username;
    private String password;

    private Session session;
    private Connection connection;

    public QueueConsumer(String brokerUrl, String username, String password) {
        this.brokerUrl = brokerUrl;
        this.username = username;
        this.password = password;
    }

    public void startReceiving(final String queueName, final MessageListener listener) throws JMSException {
        // Get the connection factory
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(username, password, brokerUrl);

        // Get the effective connection
        connection = connectionFactory.createConnection();

        // Start the connection
        connection.start();

        // Create a session
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // Create a queue if it does not exist
        Destination queue = session.createQueue(queueName);

        // Create a consumer
        MessageConsumer messageConsumer = session.createConsumer(queue);

        // Set message listener to respond to messages asynchronously
        messageConsumer.setMessageListener(listener);
    }

    public void stopReceiving() throws JMSException {
        // Close connection and session
        session.close();
        connection.close();
    }

}
